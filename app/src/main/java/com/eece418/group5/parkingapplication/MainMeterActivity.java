package com.eece418.group5.parkingapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewSwitcher;


public class MainMeterActivity extends ActionBarActivity {

    private ImageView meterImage;
    private ImageView meterImageOverlay;
    private ImageView meterImageInbetween;
    private ImageSwitcher creditcardImage;
    private TextSwitcher licensePlate;
    private TimePicker timePicker;
    private EditText lotNum;
    private TextView usrTime;

    private int[] creditCards = {R.drawable.mastercard, R.drawable.visa};
    private String[] licensePlates = {"XCG6003", "MR-DRD", "V2T1169"};
    private int numLicensePlates;
    private int numCreditCards;
    private int creditCounter;
    private int licenseCounter;
    private long duration;

    private float parkingRate = 1.25f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_meter);

        timePicker = (TimePicker) findViewById(R.id.textClock);
        lotNum = (EditText) findViewById(R.id.locationID);
        usrTime = (TextView) findViewById(R.id.userTime);

        duration = System.currentTimeMillis();

        licenseCounter = 0;
        creditCounter = 0;

        numLicensePlates = licensePlates.length;
        numCreditCards = creditCards.length;

        final Animation inRight = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        final Animation outRight = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);

        final Animation inLeft = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        final Animation outLeft = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);

        final Animation inUp = AnimationUtils.loadAnimation(this, R.anim.push_up_in);
        final Animation outUp = AnimationUtils.loadAnimation(this, R.anim.push_up_out);

        final Animation inDown = AnimationUtils.loadAnimation(this, R.anim.push_down_in);
        final Animation outDown = AnimationUtils.loadAnimation(this, R.anim.push_down_out);

        final TranslateAnimation slideLeftRight = new TranslateAnimation(0,-200,0,0);
        slideLeftRight.setDuration(1000);
        slideLeftRight.setFillAfter(true);
        slideLeftRight.setAnimationListener( new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                TranslateAnimation slide = new TranslateAnimation(-200,0,0,0);
                slide.setDuration(1000);
                slide.setFillAfter(false);
                slide.setAnimationListener( new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        if( !lotNum.getText().toString().matches("") ) {
                            FireMissilesDialogFragment confDialog = new FireMissilesDialogFragment();
                            confDialog.show(getFragmentManager(), "tag");
                        }
                        else
                            Toast.makeText(getApplicationContext(), "Please enter a lot ID number!", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                creditcardImage.startAnimation(slide);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        meterImage = (ImageView) findViewById( R.id.MeterBackground );
        meterImage.setImageResource(R.drawable.meter_image);
        meterImage.setScaleType(ImageView.ScaleType.FIT_CENTER);

        meterImageOverlay = (ImageView) findViewById( R.id.MeterBackgroundOverlay );
        meterImageOverlay.setImageResource( R.drawable.meter_image_overlay );
        meterImageOverlay.setScaleType(ImageView.ScaleType.FIT_CENTER);

        meterImageInbetween = (ImageView) findViewById( R.id.MeterBackgroundInbetween );
        meterImageInbetween.setImageResource( R.drawable.meter_image_inbetween );
        meterImageInbetween.setScaleType(ImageView.ScaleType.FIT_CENTER);

        creditcardImage = (ImageSwitcher) findViewById(R.id.creditcard);

        creditcardImage.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
                ImageView myView = new ImageView(getApplicationContext());
                return myView;
            }
        });
        creditcardImage.setImageResource(creditCards[0]);

        creditcardImage.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            public void onSwipeUp() {
//                Toast.makeText(MainMeterActivity.this, "up", Toast.LENGTH_SHORT).show();
                creditcardImage.setInAnimation(inUp);
                creditcardImage.setOutAnimation(outUp);
                creditcardImage.setImageResource(creditCards[Math.abs(++creditCounter)%numCreditCards]);
            }
            public void onSwipeRight() {
//                Toast.makeText(MainMeterActivity.this, "right", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeLeft() {
//                Toast.makeText(MainMeterActivity.this, "left", Toast.LENGTH_SHORT).show();
                creditcardImage.startAnimation(slideLeftRight);
            }
            public void onSwipeDown() {
//                Toast.makeText(MainMeterActivity.this, "down", Toast.LENGTH_SHORT).show();
                creditcardImage.setInAnimation(inDown);
                creditcardImage.setOutAnimation(outDown);
                creditcardImage.setImageResource(creditCards[Math.abs(--creditCounter)%numCreditCards]);
            }

        });


        licensePlate = (TextSwitcher) findViewById(R.id.license_plate);
        licensePlate.setFactory(new ViewSwitcher.ViewFactory() {

            @Override
            public View makeView() {

                // Create a new TextView
                TextView t = new TextView(MainMeterActivity.this);
                t.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                t.setTextAppearance(MainMeterActivity.this, android.R.style.TextAppearance_Large);
                return t;
            }
        });

        licensePlate.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            public void onSwipeUp() {
//                Toast.makeText(MainMeterActivity.this, "up", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
//                Toast.makeText(MainMeterActivity.this, "right", Toast.LENGTH_SHORT).show();
                licensePlate.setInAnimation(inLeft);
                licensePlate.setOutAnimation(outRight);
                licensePlate.setText(licensePlates[Math.abs(++licenseCounter) % numLicensePlates]);
            }

            public void onSwipeLeft() {
//                Toast.makeText(MainMeterActivity.this, "left", Toast.LENGTH_SHORT).show();
                licensePlate.setInAnimation(inRight);
                licensePlate.setOutAnimation(outLeft);
                licensePlate.setText(licensePlates[Math.abs(--licenseCounter) % numLicensePlates]);
            }

            public void onSwipeDown() {
//                Toast.makeText(MainMeterActivity.this, "down", Toast.LENGTH_SHORT).show();
            }

        });

        licensePlate.setCurrentText(licensePlates[0]);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_meter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class FireMissilesDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Confirm Parking Payment?\n" +
                                "Lot number: " + lotNum.getText() +
                                "\nParking until: " + timePicker.getCurrentHour() + " : " + timePicker.getCurrentMinute() +
                                "\nTotal Cost: $ " + String.format("%.2f",((float)timePicker.getCurrentHour() + timePicker.getCurrentMinute()/60f)*parkingRate))
                    .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            meterImage.setBackgroundColor(0xFF70FF5D);
                            duration = (System.currentTimeMillis() - duration)/1000;
                            Log.i("USERTIME", Long.toString(duration) );
                            usrTime.setText(duration + " s");
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }
}
